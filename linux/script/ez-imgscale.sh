#!/bin/env bash
# Requires: imagemagick

pikture="$1"

resolucion='100%'
if [ "$2" != "" ]
then
    resolucion=$2
fi

usage() {
printf "Usage:\n"
printf "ez-imgscale [image] [resolution] [filter]\n\n"
printf "Resolution can be WxH, area@, scale%, aspect:ratio, scaleX/scaleY%\n"
printf "If let empty, it assumes original size (100%)\n\n"
printf "Filter can be:\n"
printf "\e[1;1mb\e[1;0mox      : pixelart\n"
printf "\e[1;1mg\e[1;0maussian : general downsampling\n"
printf "\e[1;1ml\e[1;0manczos  : real life downsampling\n"
printf "\e[1;1mm\e[1;0mitchell : good overall (tensor)\n"
printf "\e[1;1ms\e[1;0cale2x   : pixelart (resolution is always 200%)"
printf "\e[1;1mr\e[1;0mobidoux : good overall (circular)\n"
printf "If let empty, it uses ImageMagick's default.\n"
exit 1
}

imgscale(){
nice magick -monitor $pikture -colorspace LAB -virtual-pixel dither -interpolate catrom $Filtro -colorspace sRGB -quality 100 $pikture
exit
}

while [ "$3" != "" ];
do
case $3 in
h | help )      usage
                ;;
b | box )       Filtro="-scale $resolucion"
                imgscale
                ;;
g | gaussian )  Filtro="-filter Gaussian -define filter:blur=0.75 -resize $resolucion"
                imgscale
                ;;
l | lanczos )   Filtro="-filter Lanczos -define filter:window=catrom -resize $resolucion"
                imgscale
                ;;
m | mitchell )  Filtro="-filter Mitchell -resize $resolucion"
                imgscale
                ;;
s | scale2x )   Filtro="-magnify"
                imgscale
                ;;
r | robidoux )  Filtro="-filter Robidoux -distort Resize $resolucion"
                imgscale
                ;;
* )             usage
esac
done

if [ "$1" == "" ]
then
    usage
fi

if [ "$3" == "" ]
then
    Filtro="-resize $resolucion"
    imgscale
fi

exit
