#!/bin/env bash

curl --progress-bar -F filedata=@$1 https://transfer.sh/ | tee /dev/null
