#!/bin/bash
# Requires: wget, jq

# This is still to be finished, but I really think it's possible

# This is for the root directory
wget -q -O- 'https://github.com/$GHuser/$GHrepo/tree/$GHbranch' | grep \"tree\" | sed 's/<script type=\"application\/json\" data-target=\"react-partial\.embeddedData\">//' | sed 's/<\/script>.*//' | head -n1 | jq '.props.initialPayload.tree.items'

# This is for a subdirectory
wget -q -O- 'https://github.com/$GHuser/$GHrepo/tree/$GHbranch/$GHdir1' | grep \"tree\" | sed 's/<script type=\"application\/json\" data-target=\"react-app\.embeddedData\">//' | sed 's/<\/script>.*//' | head -n1 | jq '.payload.tree.items'

# Now we have the list in a json format
# Sadly it truncates to the first 1000 items, since it takes the info from the website

