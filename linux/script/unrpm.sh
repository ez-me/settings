#!/bin/env bash

if ( ! command -v rpm2cpio &> /dev/null ); then
	rpm2cpio "$1" | cpio -imdv
else
    rpm2cpio.sh "$1" | cpio -imdv
fi
