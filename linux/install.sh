#!/bin/bash

echo "This will install the files in your home folder"

# Config
cp ./config/git.conf $HOME/.gitconfig

mkdir -p $HOME/.config

cp ./config/lynx.cfg $HOME/.config/lynx.cfg

cp ./config/pikaur.conf $HOME/.config/pikaur.conf

mkdir -p $HOME/.config/paru
cp ./config/paru.conf $HOME/.config/paru.conf

mkdir -p $HOME/.config/htoprc
cp ./config/htoprc $HOME/.config/htop/htoprc

mkdir -p $HOME/.config/kitty
cp ./config/kitty.conf $HOME/.config/kitty/kitty.conf

mkdir -p $HOME/.config/lsd
cp ./config/lsd.yaml $HOME/.config/lsd/config.yaml

mkdir -p $HOME/.config/nano
cp ./config/nanorc $HOME/.config/nano/nanorc


# Bin
cp ./script/bashrc.sh $HOME/.bashrc

cp ./script/zshrc.sh $HOME/.zshrc

mkdir -p $HOME/.local/bin

for i in $(ls 1 ./script/ | grep -v rc.sh)
do
  cp ./script/"$i" $HOME/.local/bin/"$i"
done

