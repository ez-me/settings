#!/bin/bash
# Requires: imagemagick jpegoptim exiv2

cd /sdcard/Pictures/"Reddit Pics"

#Remove files which we aren't gonna use
rm -f *-thumb.jpg
rm -f -- *"("*

echo "Convert disguised PNGs to JPGs"
echo "=============================="
for i in $(ls -qt | grep jpg)
do
    if [[ $(magick identify -format %[magick] "$i") == "PNG" ]]
    then
        nice magick convert "$i" -quality 100 "$i"
        echo "$i"
    fi
done

echo ""
echo "Optimizing the JPEGs..."
echo "======================="
for i in $(ls -1t | grep jpg)
do
    printf "$i\n"
    nice jpegoptim -m85 -f -p -P -s --all-normal "$i" | tail -c21 | head -c8 | sed 's/(//' | sed 's/)//'
    Imagen=$(basename "$i" .jpg)
    nice magick convert "$i" -virtual-pixel dither -interpolate catrom -scale '5%' -clamp -strip "$Imagen"-thumb.jpg
    nice jpegoptim -q -s --all-normal -m50 "$Imagen"-thumb.jpg
    exiv2 -k -it insert "$i"
    rm "$Imagen"-thumb.jpg

    touch -t $(ls --time-style="+%Y%m%d%H%M.%S" -Gghu "$i"|head -n2|tail -n1|head -c 33|tail -c16) "$i"

    mv "$i" $(basename "$i" g)eg
    printf "\n\n"
    sleep 0.1
done
rm -f *-thumb.jpg

echo ""
echo "Done"

beep && beep && beep
sleep 1
